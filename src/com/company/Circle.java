package com.company;

public class Circle {
    private double radius;
    private double length;
    private double area;
    private String colour;

    public Circle(double radius) {
        this.radius = radius;
    }

    public Circle(String colour, double radius) {
        this.colour = colour;
        this.radius = radius;
    
        area = Math.PI * (radius * radius);
        length = Math.PI * (2 * radius);

    }

    public double getRadius() {

        return radius;
    }

    public void setRadius(double radius) {

        this.radius = radius;
    }

    public double getLength() {

        return length;
    }

    public void setLength(double length) {
        this.length = length;
    }

    public double getArea() {
        return area;
    }

    public void setArea(double area) {
        this.area = area;
    }

    public String getColour() {
        return colour;
    }

    public void setColour(String colour) {
        this.colour = colour;
    }
}
